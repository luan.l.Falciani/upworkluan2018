var express = require('express');
var faker = require('faker');
var bodyParser = require('body-parser');
var expressLayouts = require('express-ejs-layouts');
var app = express();
var port = 3000;

// Definimos que vamos utilizar o ejs
app.set('view engine', 'ejs');
app.use(expressLayouts);
app.use(bodyParser.urlencoded());

// ROTAS
app.get('/', (req, res) => {
    res.render('pages/app')
});


app.use(express.static(__dirname + '/public'));
app.listen(port);
console.log('Servidor iniciado em http://localhost:' + port);
